FROM debian:jessie
MAINTAINER linuxpaul <linuxpaul@hotmail.de> 
 
# prepare apt proxy & disable interactive installation
# ADD 00proxy /etc/apt/apt.conf.d/

ENV MYSQL_ROOT_PASSWORD sqlpwd
ENV MYSQL_DATABASE owncloud
ENV MYSQL_USER OC_DB_ADM
ENV MYSQL_PASSWORD OcDbPassWd
ENV MYSQL_ROOT_HOST %

ENV DEBIAN_FRONTEND noninteractive

# Get MySQL
RUN apt-get update && apt-get install -y -qq --no-install-recommends mysql-server; apt-get clean

#ENV PATH $PATH:/usr/local/bin:/usr/local/mysql/bin:/usr/local/mysql/scripts
 
# Open MySQL to a world
RUN sed -i -e"s/^bind-address\s*=\s*127\.0\.0\.1/bind-address = 0\.0\.0\.0/" /etc/mysql/my.cnf
 
RUN mkdir /docker-entrypoint-initdb.d

VOLUME /var/lib/mysql

COPY docker-entrypoint.sh /usr/local/bin/
RUN ln -s usr/local/bin/docker-entrypoint.sh /entrypoint.sh # backwards compat
ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 3306 
CMD ["mysqld"]